let encodeurResultat = document.querySelector("#encodeurResultat");
let encodeurBinaire = document.querySelector("#encodeurBinaire");
let encodeur = document.querySelector("#encodeur");
let selecteurEncodeur = document.querySelectorAll("#encodageConteneur input[type=radio]");

let decodeurResultat = document.querySelector("#decodeurResultat");
let decodeurBinaire = document.querySelector("#decodeurBinaire");
let decodeur = document.querySelector("#encodeurBinaire");
let selecteurDecodeur = document.querySelectorAll("#decodageConteneur input[type=radio]");

let selecteurNumeration = document.querySelectorAll("#encodageNumeration input[type=radio]");

function bin(texte, size){
  let codes = [];
  for (let i = 0; i < texte.length; i++){
    let codePoint = texte.charCodeAt(i);
    if (codePoint < (2 ** size)){
      codes.push(codePoint.toString(2).padStart(size, '0'));
    } else {
      return null;
    }
  }
  return codes;
}

function binUTF(texte){
  let codes = [];
  for (let i = 0; i < texte.length; i++){
    let codePoint = texte.charCodeAt(i);
    let codePointBinaire = codePoint.toString(2);
    let p = codePointBinaire.length;
    if (p < 8){
      codes.push(codePointBinaire.padStart(8, '0'));
    } else if (p < 12){
      codePointBinaire = codePointBinaire.padStart(11, '0');
      codes.push("110" + codePointBinaire.substring(0, 5));
      codes.push("10" + codePointBinaire.substring(5));
    } else if (p < 17){
      codePointBinaire = codePointBinaire.padStart(16, '0');
      codes.push("1110" + codePointBinaire.substring(0, 4));
      codes.push("10" + codePointBinaire.substring(4, 10));
      codes.push("10" + codePointBinaire.substring(10));
    } else {
      codePointBinaire = codePointBinaire.padStart(21, '0');
      codes.push("11110" + codePointBinaire.substring(0, 3));
      codes.push("10" + codePointBinaire.substring(3, 9));
      codes.push("10" + codePointBinaire.substring(9, 15));
      codes.push("10" + codePointBinaire.substring(15));
    }
  }
  return codes;
}

function encodeASCII(texte, base){
  let codes = [];
  for (let i = 0; i < texte.length; i++){
    let codePoint = texte.charCodeAt(i);
    if (codePoint < 127){
      codes.push(codePoint.toString(base));
    } else {
      return null;
    }
  }
  return codes;
}

function encodeISO(texte, base){
  let codes = [];
  for (let i = 0; i < texte.length; i++){
    let codePoint = texte.charCodeAt(i);
    if (codePoint < 256){
      codes.push(codePoint.toString(base));
    } else {
      return null;
    }
  }
  return codes;
}

function encodeUTF(texte, base){
  let codes = [];
  for (let i = 0; i < texte.length; i++){
    codes.push(texte.charCodeAt(i).toString(base));
  }
  return codes;
}

function encode(){
  let code = null;
  let binaire = null;
  let encodage = document.querySelector("#encodageConteneur input[type=radio]:checked");
  let numeration = parseInt(document.querySelector("#encodageNumeration input[type=radio]:checked").value, 10);
  switch (encodage.value){
    case "ASCII" :
      code = encodeASCII(encodeur.value, numeration);
      if (code != null){
        code = code.join(' ');
      }
      binaire = bin(encodeur.value, 7);
      if (binaire != null){
        binaire = binaire.join(' ');
      }
      break;
    case "ISO" :
      code = encodeISO(encodeur.value, numeration);
      if (code != null){
        code = code.join(' ');
      }
      binaire = bin(encodeur.value, 8).join(' ');
      break;
    case "UTF" :
      code = encodeUTF(encodeur.value, numeration);
      if (code != null){
        code = code.join(' ');
      }
      binaire = binUTF(encodeur.value).join(' ');
      break;
  }
  encodeurResultat.value = (code == null)?"IMPOSSIBLE":code;
  encodeurBinaire.value = (binaire == null)?"IMPOSSIBLE":binaire;
}

function bitsToCodePointASCII(bits){
  let codes = [];
  for (let i = 0; i < bits.length; i++){
    if (bits[i].length < 8 || bits[i][0] == "0"){
      codes.push(bits[i]);
    } else {
      return null;
    }
  }
  return codes;
}

function bitsToCodePointUTF(bits){
  let codes = [];
  let i = 0;

  while (i < bits.length){
    if (bits[i][0] == "0"){
      codes.push(bits[i]);
      i++;
    } else {
      let p = bits[i].split("0")[0].length;
      try {
      switch (p){
        case 2:
          codes.push(bits[i].substring(3) + bits[i + 1].substring(2));
          break;
        case 3:
          codes.push(bits[i].substring(4) + bits[i + 1].substring(2) + bits[i + 2].substring(2));
          break;
        case 4:
          codes.push(bits[i].substring(5) + bits[i + 1].substring(2) + bits[i + 2].substring(2) + bits[i + 3].substring(2));
          break;
      }
    } catch(error){
      return null;
    }
      i = i + p;
    }
  }

  return codes;
}

function decode(){
  let bits = null;
  let mots = "";

  if (decodeur.value == "IMPOSSIBLE"){
    mots = "IMPOSSIBLE";
  } else {
    let decodage = document.querySelector("#decodageConteneur input[type=radio]:checked");
    let data = decodeur.value.split(' ');
    data = (data.length >= 1 && data[0].length > 0)?data:[];
    switch (decodage.value){
      case "ASCII" :
        bits = bitsToCodePointASCII(data);
        break;
      case "ISO" :
        bits = data;
        break;
      case "UTF" :
        bits = bitsToCodePointUTF(data);
        break;
    }
    if (bits != null && bits.length > 0){
      for (let i = 0; i < bits.length; i++){
        let codePoint = parseInt(bits[i], 2);
        mots += String.fromCodePoint(codePoint);
      }
    } else {
      mots = "IMPOSSIBLE";
    }
  }
  decodeurBinaire.value = mots;
}

encodeur.addEventListener('keyup', function(){
  encode();
  decode();
});
selecteurEncodeur.forEach((item, i) => {
  item.addEventListener('change', function(){
    let encodage = document.querySelector("#encodageConteneur input[type=radio]:checked");
    switch (encodage.value){
      case "ASCII" :
        document.querySelector("#table").src = "https://philippe-boddaert.gitlab.io/encodage/assets/ascii.png";
        break;
      case "ISO" :
        document.querySelector("#table").src = "https://philippe-boddaert.gitlab.io/encodage/assets/iso-8859-1.png";
        break;
      case "UTF" :
        document.querySelector("#table").src = "https://philippe-boddaert.gitlab.io/encodage/assets/iso-8859-1.png";
        break;
    }
    encode();
    decode();
  });
});

selecteurDecodeur.forEach((item, i) => {
  item.addEventListener('change', function(){
    encode();
    decode();
  });
});

selecteurNumeration.forEach((item, i) => {
  item.addEventListener('change', function(){
    encode();
  });
});
